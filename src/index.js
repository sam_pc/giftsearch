import React from 'react';
import ReactDOM from 'react-dom';

import { App } from './views/App/App';

import 'semantic-ui-css/semantic.min.css'
import 'animate.css'

ReactDOM.render(<App />, document.getElementById('root'));
