import React, { useState } from 'react';
import { Container, Header, List } from 'semantic-ui-react';

import { AddCategory } from '../../components/AddCategory';
import { GiftGrid } from '../../components/GiftGrid';
import styles from './App.css';

export const App = () => {
  const [categories, setCategories] = useState(['Psychedelic']);
  return (
    <Container className={styles.container}>
      <div className="title">
        <Header as="h1"> GiftSearch </Header>
        <img
          className="mark"
          src="https://www.gstatic.com/tenor/web/attribution/PB_tenor_logo_grey_horizontal.svg"
          alt="mark"
        />
      </div>
      <AddCategory setCategories={setCategories} />
      <hr></hr>
      <List>
        {categories.map((category) => (
          <GiftGrid category={category} key={category}></GiftGrid>
        ))}
      </List>
    </Container>
  );
};
