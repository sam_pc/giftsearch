import apiRequest from './apiRequest';

class Gif {
  constructor(id, title, url) {
    this.id = id;
    this.title = title;
    this.url = url;
  }
}

export const getGifsByCategory = async (category) => {
  const url = `https://api.giphy.com/v1/gifs/search?q=${ encodeURI(category)}&limit=10&api_key=A8xMXqzieIHmtO3BjGLAtf1daSSDAv8K`;
  return apiRequest(url).then((body) =>
    body.data.map(
      (gif) =>
        new Gif(gif.id, gif.title, gif.images.fixed_height_downsampled.url)
    )
  );
};
