export default async (url) =>
  await fetch(url, { mode: 'cors' }).then((r) => r.json());
