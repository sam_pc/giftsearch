import { useState, useEffect } from 'react';

import { getGifsByCategory } from '../helpers/gifs.helper';

export const useGetGifs = (category) => {
  const [state, setState] = useState({ gifs: [], isLoading: true });
  useEffect(() => {
    getGifsByCategory(category).then((gifs) =>
      setState({ gifs, isLoading: false })
    );
  }, [category]);

  return state;
};
