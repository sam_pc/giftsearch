import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'semantic-ui-react';

import './AddCategory.css';

export const AddCategory = ({ setCategories }) => {
  const [newCategory, setNewCategory] = useState('');

  const handleInputChange = (e) => setNewCategory(e.target.value);

  const handleFormSubmit = (e) => {
    e.preventDefault();
    if (newCategory.trim().length >= 3) {
      setCategories((categories) => [newCategory, ...categories]);
      setNewCategory('');
    }
  };

  return (
    <Form className='form' onSubmit={handleFormSubmit}>
      <Input
        value={newCategory}
        onChange={handleInputChange}
        placeholder="Add Category"
        fluid
      ></Input>
    </Form>
  );
};

AddCategory.propTypes = {
  setCategories: PropTypes.func.isRequired,
};
