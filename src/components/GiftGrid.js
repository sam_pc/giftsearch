import React from 'react';
import PropTypes from 'prop-types';
import { Header, Card, Loader } from 'semantic-ui-react';

import { GifGridItem } from './GifGridItem';

import { useGetGifs } from '../hooks/useGetGifs';

import './GiftGrid.css';

export const GiftGrid = ({ category }) => {
  const { gifs, isLoading } = useGetGifs(category);

  return (
    <section className="gifs">
      <Header as="h2">{category}</Header>
      <Loader className="animate__animated animate__flash" inline='centered' active={isLoading}>Loading</Loader>
      <Card.Group className="gifs__grid animate__animated animate__backInUp" itemsPerRow={4} doubling>
        {gifs.map((gif) => (
          <GifGridItem {...gif} key={gif.id} />
        ))}
      </Card.Group>
    </section>
  );
};

GiftGrid.propTypes = {
  category: PropTypes.string.isRequired,
};
