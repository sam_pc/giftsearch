import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image } from 'semantic-ui-react';

import './GifGridItem.css';

export const GifGridItem = ({ title, url }) => {
  return (
    <Card raised>
      <Image height="70%" ui={false} src={url} alt={title} />
      <Card.Content>
        <Card.Header> {title}</Card.Header>{' '}
      </Card.Content>
    </Card>
  );
};

GifGridItem.propTypes = {
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};
